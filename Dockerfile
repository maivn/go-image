FROM golang

RUN apt-get update
RUN apt-get install -y libreoffice-writer

RUN go get "gitlab.com/maivn/mapper"
RUN go get "gitlab.com/maivn/amogo/amogo"
RUN go get "github.com/gorilla/websocket"
RUN go get "gitlab.com/maivn/custom_form"
RUN go get "github.com/iancoleman/strcase"
RUN go get "github.com/gorilla/schema"
RUN go get "github.com/docker/docker/client"
RUN go get "github.com/docker/docker/api/types"
RUN go get "github.com/streadway/amqp"
RUN go get "github.com/spf13/viper"
RUN go get "github.com/unidoc/unioffice"
RUN go build -i github.com/unidoc/unioffice/...
RUN go get "golang.org/x/net/publicsuffix"
RUN go get "github.com/go-redis/redis"
RUN go get "github.com/lib/pq"
RUN go get "gitlab.com/maivn/rutime"
RUN go get "github.com/jinzhu/gorm"
RUN go get "github.com/gin-gonic/gin"
RUN go get "github.com/json-iterator/go"
RUN go get "github.com/joho/godotenv"
RUN go get "github.com/satori/go.uuid"
RUN go get "gopkg.in/go-playground/validator.v9"
RUN go get "github.com/dgrijalva/jwt-go"

EXPOSE 8080